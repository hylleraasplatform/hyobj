# hyobj.constants

Constants provide constants to be used with the hylleraas software platform.
Currently, values are adapted from [qcelemental](https://github.com/MolSSI/QCElemental).
Usage:

```python

from hyobj import Constants
c = Constants.c_au
b2a = Constants.bohr2angstroms
```
