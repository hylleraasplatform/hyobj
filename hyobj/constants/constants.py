from typing import Final

from qcelemental import PhysicalConstantsContext

Constants: Final = PhysicalConstantsContext('CODATA2018')
