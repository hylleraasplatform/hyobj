from __future__ import annotations

import re
from dataclasses import dataclass
from functools import singledispatchmethod
from typing import Any, Optional, Tuple, Union

quantity_like = Union['Quantity', Tuple[str, float], dict]


@dataclass
class Quantity:
    """Quantity class.

    Parameters
    ----------
    unit : str
        unit of quantity.
    value : float
        Value of quantity wrt atomic units.
    symbol : Optional[str], optional
        Symbol of quantity, by default unit

    """

    unit: str
    value: Optional[float] = None
    symbol: Optional[str] = None

    def __post_init__(self):
        """Post init."""
        if self.value is None:
            try:
                d = self.generate(self.unit).__dict__
            except AttributeError as e:
                raise AttributeError(
                    f'Value must be given to unit {self.unit}.') from e
            else:
                self.__dict__.update(d)
        if self.symbol is None:
            self.symbol = self.unit

    @singledispatchmethod
    def __getitem__(self, key: Union[str, int]) -> Union[str, float]:
        """Get item."""
        raise TypeError(f'Unknown key type: {type(key)}')

    @__getitem__.register(str)
    def _(self, key: str) -> Union[str, float]:
        """Get item."""
        return getattr(self, key)

    @__getitem__.register(int)
    def _(self, key: int) -> Union[str, float]:
        """Get item."""
        int_map = {0: 'unit', 1: 'value', 2: 'symbol'}
        return getattr(self, int_map[key]) if key < 3 else None

    @singledispatchmethod
    @classmethod
    def generate(cls, q: quantity_like):
        """Generate quantity."""
        try:
            return cls.generate(q.__dict__)
        except AttributeError as e:
            raise TypeError(f'Unknown quantity: {q} with type '
                            f'{type(q)}') from e

    @generate.register(tuple)
    @classmethod
    def _(cls, q: Tuple[str, float]):
        """Generate quantity."""
        try:
            return cls(*(str(q[0]),
                         float(q[1]),
                         *q[2:]))
        except IndexError as e:
            raise IndexError('Tuple must have at least 2 elements.') from e

    @generate.register(dict)
    @classmethod
    def _(cls, q: dict):
        """Generate quantity."""
        unit_keys = ['unit', 'name', 'unit_name']
        value_keys = ['value', 'val', 'factor', 'conversion_factor']
        q_unit = next((q.get(k) for k in unit_keys), None)
        q_value = next((q.get(k) for k in value_keys), None)
        q_symbol = q.get('symbol')

        try:
            return cls(unit=str(q_unit),
                       value=float(q_value),
                       symbol=q_symbol)
        except (KeyError, TypeError) as e:
            raise KeyError(
                'Dict must have at least "unit" and "value" keys, ' +
                f'got {q.keys()}') from e

    @generate.register(str)
    @classmethod
    def _(cls, s: str):
        """Generate quantity."""
        s = re.sub(r'^Quantity\((.*)\)$', r'\1', s.strip())
        d = {k.strip(): v.strip()
             for k, v in (pair.split('=')
                          for pair in s.replace("'", '').split(','))}
        return cls(unit=str(d.get('unit')),
                   value=float(d.get('value')),
                   symbol=str(d.get('symbol')))

    @classmethod
    def from_dict(cls, q: quantity_like):
        """Generate quantity."""
        return cls.generate(q)

    @classmethod
    def from_tuple(cls, q: quantity_like):
        """Generate quantity."""
        return cls.generate(q)

    @classmethod
    def from_str(cls, q: quantity_like):
        """Generate quantity."""
        return cls.generate(q)

    def __eq__(self, other: Any) -> bool:
        """Check equality."""
        try:
            return self.unit == other.unit and self.value == other.value
        except AttributeError:
            return False

    def __neq__(self, other: Any) -> bool:
        """Check inequality."""
        return not self.__eq__(other)
