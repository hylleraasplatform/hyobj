from __future__ import annotations

import re
from copy import deepcopy
from typing import Any

import yaml
from jinja2 import Environment, PackageLoader

from ..constants import Constants
from .names import get_quantity_name as gqn
from .quantity import Quantity

UNITS_DICT = yaml.full_load(
    Environment(loader=PackageLoader('hyobj', 'units'),
                autoescape=True).get_template(
        'units.yml').render(Constants=Constants))


class Units:
    """Units class for hylleraas objects.

    Parameters
    ----------
    style: str
        units style: 'atomic', 'real', 'si', or 'metal', 'angstrom', 'bohr'
        'atomic' is default.
    kwargs: dict
        Additional units to add or replace.

    Notes
    -----
    The units are all relative Units('atomic').

    """

    def __init__(self, *args, **kwargs):
        """Set subclass attributes."""
        # self.debug = kwargs.pop('debug', False)
        if len(args) > 1:
            raise IndexError('Only one positional argument <style> allowed')
        style = args[0] if len(args) == 1 else kwargs.pop('style', None)

        style = ('atomic' if style is None
                 and not kwargs else style)

        if isinstance(style, Units):
            self.__dict__.update(style.__dict__)
        elif isinstance(style, str):
            units_to_use: dict = UNITS_DICT.get(style.lower(),
                                                getattr(self.from_str(
                                                    style), '__dict__',
                                                None))
            if units_to_use is not None:
                self.__dict__.update(units_to_use)
        elif isinstance(style, dict):
            self.__dict__.update(style)
            style = 'custom'
        elif style is not None:
            raise TypeError(f'Unknown units style: {style}' +
                            f' with type {type(style)}'
                            ' (must be str, Units, or dict)')

        for k, v in kwargs.items():
            setattr(self, k, v)

        self._set_quantities()

    def _set_quantities(self):
        """Set quantities."""
        for k, v in self.__dict__.items():
            if k in ['style', 'debug'] or v is None:
                continue
            setattr(self, k, Quantity.generate(v))

        if all(getattr(self, key, None) is not None
               for key in ['energy', 'length']):
            self._set_force()
            self._set_hessian()

    def _set_force(self):
        """Set force."""
        if getattr(self, 'force', None) is not None:
            return

        force_dict = {
            'unit': f'{self.energy.unit}/{self.length.unit}',
            'value': self.energy.value / self.length.value,
            'symbol': f'{self.energy.symbol}/{self.length.symbol}',
        }
        self.force = Quantity(**force_dict)

    def _set_hessian(self):
        """Set hessian."""
        if getattr(self, 'hessian', None) is not None:
            return

        hessian_dict = {
            'unit': f'{self.energy.unit}/{self.length.unit}**2',
            'value': self.energy.value / self.length.value**2,
            'symbol': f'{self.energy.symbol}/{self.length.symbol}**2',
        }
        self.hessian = Quantity(**hessian_dict)

    def __repr__(self):  # pragma: no cover
        """Repr."""
        d = {k: v for k, v in self.__dict__.items()
             if k not in ['style', 'debug'] or v is not None}
        return f'Units({d})'

    def __str__(self):  # pragma: no cover
        """Str."""
        return self.__repr__()

    def __add__(self, other, overwrite=False):
        """Add units."""
        u = deepcopy(self)
        q = [k for k in self.__dict__.keys() if k not in ['style', 'debug']]
        if any(getattr(other, k, None) is not None for k in q):
            if not overwrite:
                raise ValueError(f'Units already have {q}')
        u.__dict__.update(other.__dict__)
        return Units(u)

    def replace(self, **kwargs):
        """Replace units."""
        u = deepcopy(self)
        # u = replace(self)
        u.__dict__.update(kwargs)
        return Units(u)

    def update(self, **kwargs):
        """Update units."""
        self.__dict__.update(kwargs)
        self._set_quantities()

    @classmethod
    def get_quantity_name(cls, name: str) -> str:
        """Get name of quantity."""
        return gqn(name)

    @classmethod
    def get_quantity(cls, name: str, units) -> Quantity:
        """Get unit of quantity."""
        name_q = cls.get_quantity_name(name)
        return getattr(units, name_q, None) if name_q is not None else None

    @classmethod
    def from_dict(cls, d: dict) -> Units:
        """Create units from dict."""
        return cls(**d)

    @classmethod
    def from_str(cls, s: str) -> Units:
        """Create units from __str__ representation."""
        if 'Units(' not in s:
            return None
        s = re.sub(r'Units\(|\{|\}|\)$', '', s)
        vals = s.replace(", '", "; '").split(';')
        try:
            d = {key.strip().replace("'", ''): Quantity.generate(value)
                 for val in vals
                 for key, value in [val.split(':')]}
        except ValueError:
            # empty string
            return None
        return cls(**d)

    def __eq__(self, other: Any) -> bool:
        """Compare units."""
        try:
            return self.__dict__ == other.__dict__
        except AttributeError:
            return False
