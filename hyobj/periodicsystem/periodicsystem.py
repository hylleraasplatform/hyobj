from __future__ import annotations

import hashlib
from pathlib import Path
from typing import Any, Dict, List, Optional, Union

import numpy as np

try:
    from MDAnalysis.lib import distances, mdamath

    mda = True
except ImportError:  # pragma: no cover
    mda = None

# from ...constants import Constants
# from qcelemental import PhysicalConstantsContext

from ..molecule import Molecule, MoleculeLike
from ..units import Units

# constants = PhysicalConstantsContext('CODATA2018')
# def settable_property(s):
#     """See see https://github.com/python/mypy/issues/9160."""
#     def mysetattr(self, value):
#         name = s.__name__
#         if hasattr(self, f'_{name}'):
#             setattr(self, f'_{name}', value)
#         else:
#             super(self.__class__, self).__setattr__(name, value)

#     return property(s, mysetattr)


class PeriodicSystem(Molecule):
    """A class representing a periodic system.

    This class extends the `MoleculeLike` class and adds support
    for periodic boundary conditions.

    Attributes
    ----------
        system (Any): The system to represent.
        pbc (np.ndarray): The periodic boundary conditions.
        unit (str): The unit of length used for the periodic boundary
                    conditions.

    """

    def __init__(
        self,
        molecule: Union[MoleculeLike, str, Path, Dict],
        pbc: Optional[Union[list, np.ndarray]] = None,
        **kwargs,
    ):
        try:
            super().__init__(molecule, **kwargs)
        except ValueError:
            raise ValueError(
                'could not construct Molecule object from ' + f'{molecule}'
            )

        if mda is None:
            raise ImportError('could not find package MDAnalysis')
        if kwargs.get('unit'):
            raise DeprecationWarning('unit is deprecated, use units instead')

        length_conv = 1.0

        pbc = (
            self.get_pbc(self._mol_input, length_conv) if pbc is None else pbc
        )
        coordinates = self.get_coordinates(length_conv)
        # self.unit = self.systen.properties.get('unit', 'angstrom')

        if np.array(pbc).shape == (6,):
            self._pbc = self.to_box(pbc)
        elif np.array(pbc).shape == (9,):
            self._pbc = self.reset_box(np.array(pbc).ravel().reshape(3, 3))
        elif np.array(pbc).shape == (3, 3):
            self._pbc = self.reset_box(np.array(pbc).ravel().reshape(3, 3))
        else:
            raise ValueError(
                'expected unit cell input or triclinic vectors'
                + f'but got {np.array(pbc).shape}'
            )

        if np.linalg.norm(pbc) < 1e-4:
            raise ValueError(
                'could not find finite ' + 'periodic boundary conditions'
            )

        self._coordinates = self.apply_pbc(coordinates, self._pbc)
        self._fcoordinates = self.get_fcoordinates(
            self._coordinates, self._pbc
        )
        # self._properties = self.system.properties

    def get_pbc(self, system: Any, fac: Optional[float] = 1.0) -> np.ndarray:
        """Get periodic boundary condition from system."""
        pbc = np.zeros((3, 3))
        if isinstance(system, PeriodicSystem):
            pbc = system._pbc
        elif isinstance(system, Molecule):
            pbc = system.properties.get('unit_cell')
        elif isinstance(system, dict):
            unit_cell_keys = ['unit_cell', 'box', 'cell', 'pbc']
            for key in unit_cell_keys:
                if key in system.keys():
                    pbc = system[key]
                    break
            if 'properties' in system.keys():
                pbc = system['properties'].get('unit_cell')
        elif hasattr(system, 'pbc'):
            pbc = system.pbc
        elif hasattr(system, 'unit_cell'):
            pbc = system.unit_cell  # type: ignore
        elif hasattr(system, 'box'):
            pbc = system.box  # type: ignore
        else:
            pbc = np.zeros(3, 3)

        return np.array(pbc) * fac

    @property
    def hmatrix(self):
        """Get periodic boundary conditions."""
        return self.pbc

    @property
    def box(self):
        """Get periodic boundary conditions."""
        return self.pbc

    @property
    def units(self):
        """Get units."""
        return self._units

    @units.setter
    def units(self, value: Units):
        """Set units."""
        length_conv = value.length[1] / self._units.length[1]  # type: ignore
        pbc = self._pbc * length_conv
        coordinates = self._coordinates * length_conv
        self._pbc = np.array(pbc).ravel().reshape(3, 3)
        self._coordinates = self.apply_pbc(coordinates, self._pbc)
        self._fcoordinates = self.get_fcoordinates(
            self._coordinates, self._pbc
        )
        self._units = value

    @property
    def pbc(self):
        """Get periodic boundary conditions."""
        return self._pbc

    @pbc.setter
    def pbc(self, value):
        """Set periodic boundary conditions."""
        self._pbc = np.array(value).ravel().reshape(3, 3)
        self._coordinates = self.apply_pbc(self.get_coordinates(), self._pbc)
        self._fcoordinates = self.get_fcoordinates(
            self._coordinates, self._pbc
        )

    def reset_box(self, pbc: np.ndarray) -> np.ndarray:
        """Reset PBC to account for rotations."""
        tv = self.to_triclinic(pbc)
        return self.to_box(tv)

    def get_coordinates(self, fac: Optional[float] = 1.0) -> np.ndarray:
        """Get coordinates from self.systems."""
        # if isinstance(self.system, MoleculeLike):
        #     return np.array(self.system.coordinates).
        # ravel().reshape(-1, 3)*fac
        # else:
        #     return None
        return np.array(self.coordinates).ravel().reshape(-1, 3) * fac

    def to_triclinic(self, pbc: np.ndarray) -> List[float]:
        """Convert unit cell to triclinic vectors."""
        pbc = np.array(pbc).ravel().reshape(3, 3)
        lx, ly, lz, alpha, beta, gamma = mdamath.triclinic_box(*pbc)
        return [lx, ly, lz, alpha, beta, gamma]

    def to_box(self, triclinic_vectors: List[float]) -> np.ndarray:
        """Convert triclinic vectors to unit cell."""
        pbc = mdamath.triclinic_vectors(triclinic_vectors)
        return np.array(pbc).ravel().reshape(3, 3)

    def apply_pbc(
        self, coordinates: np.ndarray, pbc: np.ndarray
    ) -> np.ndarray:
        """Apply periodic boundary conditions."""
        if coordinates is None:
            return coordinates

        return np.array(
            distances.apply_PBC(coordinates, self.to_triclinic(pbc))
        ).ravel()

    def get_fcoordinates(
        self, coordinates: np.ndarray, pbc: np.ndarray
    ) -> np.ndarray:
        """Compute fractional coordinates."""
        h0_inv = np.linalg.inv(np.array(pbc).ravel().reshape(3, 3))
        fcoord = [
            h0_inv @ c for c in np.array(coordinates).ravel().reshape(-1, 3)
        ]
        return np.array(fcoord)

    def get_ase_object(self):
        """Get ASE atoms object."""
        try:
            from ase import Atoms
        except ImportError:
            raise ImportError('ase is required for this feature')
        return Atoms(
            symbols=self.atoms,
            positions=self.coordinates,
            pbc=True,
            cell=self.pbc,
        )

    def apply_pbc_fcoord(self, fcoord: np.ndarray) -> np.ndarray:
        """Apply pbc to fractional coordiantes."""
        flat = np.array(fcoord).ravel()
        for i in range(len(flat)):
            if flat[i] >= 1.0:
                flat[i] -= 1.0
            if flat[i] < 0.0:
                flat[i] += 1.0
        return flat.reshape(-1, 3)

    def fcoord2coord(self, fcoord: np.ndarray, pbc: np.ndarray) -> np.ndarray:
        """Convert fractional coordinates to normal coordinates."""
        coord = [pbc @ fc for fc in self.apply_pbc_fcoord(fcoord)]
        return np.array(coord).ravel().reshape(-1, 3)

    def __repr__(self):
        """Dataclass-like representation."""
        keywords = [f'{key}={value!r}' for key, value in self.__dict__.items()]
        return '{}({})'.format(type(self).__name__, ', '.join(keywords))

    # @property
    # def atoms(self):
    #     """Get atoms."""
    #     if isinstance(self.system, MoleculeLike):
    #         return self.system.atoms
    #     else:
    #         return []

    # @property
    # def num_atoms(self):
    #     """Get number of atoms."""
    #     return len(self.atoms)

    @property
    def xyz_string(self):
        """Get extended xyz string."""
        # TODO: Maybe add energy and forces?
        ret_str = f'{self.num_atoms}\n'
        # write lattice vectors
        ret_str += (
            f'Lattice="{self.pbc[0, 0]} {self.pbc[0, 1]} '
            + f'{self.pbc[0, 2]} {self.pbc[1, 0]} {self.pbc[1, 1]} '
            + f'{self.pbc[1, 2]} {self.pbc[2, 0]} {self.pbc[2, 1]} '
            + f'{self.pbc[2, 2]}" Properties=species:S:1:pos:R:3 '
            + 'pbc="T T T"\n'
        )
        # write atoms
        for i, atom in enumerate(self.atoms):
            ret_str += (
                f'{atom} {self.coordinates[i, 0]} '
                + f'{self.coordinates[i, 1]} {self.coordinates[i, 2]}\n'
            )
        return ret_str

    # @property
    # def properties(self):
    #     """Get Molecule properties."""
    #     # if isinstance(self.system, MoleculeLike):
    #     #     return self.system._properties
    #     # else:
    #     #     return {}
    #     try:
    #         return self.system.properties
    #     except AttributeError:
    #         return {}

    @property
    def hash(self):
        """Get hash."""
        return hashlib.sha256(
            ''.join(self.xyz_string).encode('utf-8')
        ).hexdigest()

    @property
    def coordinates(self):
        """Get coordinates."""
        return np.array(self._coordinates).ravel().reshape(-1, 3)

    @coordinates.setter
    def coordinates(self, value):
        """Set coordinates."""
        self.pbc = self.reset_box(self.pbc)
        self._coordinates = self.apply_pbc(np.array(value), self.pbc)
        self._fcoordinates = self.get_fcoordinates(self._coordinates, self.pbc)

    @property
    def fractional_coordinates(self):
        """Get fractional coordinates."""
        return self._fcoordinates

    @fractional_coordinates.setter
    def fractional_coordinates(self, value):
        """Set fractional coordinates."""
        self.pbc = self.reset_box(self.pbc)
        self._fcoordinates = self.apply_pbc_fcoord(np.array(value))
        self._coordinates = self.fcoord2coord(self._fcoordinates, self.pbc)
