from pathlib import Path
from typing import Tuple

try:
    import ase
except ImportError:
    ase = None

import pandas as pd


class TrajectoryInterface:
    """Interface to Pandas."""

    def __init__(self):
        """Initialise trajecotry interface."""
        if ase is None:
            raise ImportError('could not import ase')

    def get_data(self, inp: Path, options: dict) -> pd.DataFrame:
        """Get data.

        Parameter
        ---------
        inp:  pathlib.Path
            path to unput file
        options: dict
            unused

        Returns
        -------
        pandas.DataFrame
            pandas DataFrame

        """
        filename = Path(inp)
        if not filename.exists():
            raise FileNotFoundError(f'could not find file {filename}')
        input_suffix = filename.suffix
        if input_suffix == '.xyz':
            fmt = 'extxyz'
        else:
            fmt = input_suffix[1:]
        traj = ase.io.read(filename, index=':', format=fmt)
        data_list = []
        for atoms in traj:
            arrays_dict = atoms.arrays.copy()
            data_dict = {
                **arrays_dict,
                **atoms.info,
            }

            data_dict['atoms'] = atoms.get_chemical_symbols()
            # check if cell is present
            if atoms.get_pbc().any():
                data_dict['cell'] = atoms.get_cell().array

            # try to get energy, forces and stress
            try:
                data_dict['energy'] = atoms.get_potential_energy()
            except Exception:
                pass

            try:
                data_dict['forces'] = atoms.get_forces()
            except Exception:
                pass

            try:
                data_dict['stress'] = atoms.get_stress()
            except Exception:
                pass

            data_list.append(data_dict)

        df = pd.DataFrame(data_list)
        if 'numbers' in df.columns:
            # remove atomic numbers
            df = df.drop(columns=['numbers'])

        return df

    def get_shape(self, df: pd.DataFrame) -> Tuple[int]:
        """Get shape of DataFrame."""
        return df.shape

    def to_pandas(self, data, *args):
        """Convert to pandas."""
        return data
