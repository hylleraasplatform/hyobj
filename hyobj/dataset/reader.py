# import warnings
from functools import singledispatchmethod
from pathlib import Path
from typing import Any, Dict, List, Union

import numpy as np
import pandas as pd

from .abc import BaseInterface, data_type
from .numpy import NumpyInterface
from .pandas import PandasInterface
from .trajectory import TrajectoryInterface


class Reader:
    """Methods for reading data."""

    @singledispatchmethod
    def get_data(self, data, **kwargs):
        """Get data from input."""
        try:
            return self.get_data(data.__dict__, **kwargs)
        except AttributeError as e:
            raise TypeError(f'Unknown type: {type(data)}') from e

    @get_data.register(pd.DataFrame)
    def _(self, data: pd.DataFrame, **kwargs):
        """Get data from input."""
        return data

    @get_data.register(pd.Series)
    def _(self, data: pd.Series, **kwargs):
        return data.to_frame()  # type: ignore

    @get_data.register(list)
    def _(self, data: list, **kwargs):
        """Get data from list."""
        if not all(isinstance(d, dict) for d in data):
            try:
                s = [d.__dict__ for d in data]
            except AttributeError as e:
                raise TypeError(f'Unknown type: {type(data)}') from e
        else:
            s = data

        d = {k: [ss.get(k) for ss in s] for k in s[0].keys()}
        return self.get_data(d, **kwargs)

    @get_data.register(dict)
    def _(self, data: dict, **kwargs):
        """Convert a dictionary into a pandas DataFrame.

        Arguments
        ---------
        data (dict): The input dictionary to be converted.
        **kwargs: Additional keyword arguments.

        Returns
        -------
        pandas.DataFrame: The converted DataFrame.

        Raises
        ------
        ValueError: If the values in the dictionary are not of the same length.
        ValueError: If the values in the dictionary are not supported types.

        """
        out_dict = {}
        # Check if it is a single entry
        length = 0
        array_types = (np.ndarray, List)
        single_types = (str, float, int, bool, Path)
        keys_select = kwargs.get('keys', data.keys())
        # for k in keys_select:
        #     if k not in data:
        #         print(f'ERROR: Key {k} not found in data {data.keys()}')
        #         # keys_select.pop(i)
        for k, v in data.items():
            if k not in keys_select:
                continue

            if isinstance(v, single_types):
                length = 1
                break
            elif isinstance(v, array_types):
                if len(v) == 1:
                    length = 1
                    break
        # check if all are 1d lists, then length is 1
        if length != 1:
            is_1d_list_only = sum(
                [not isinstance(v[0], array_types) for v in data.values()]
            ) == len(data)
            if is_1d_list_only:
                length = 1

        # Now, check if all dictionary elements are of same length
        if length != 1:
            lengths = [len(v) for v in data.values()]
            if len(set(lengths)) != 1:
                raise ValueError(
                    f'All values in dict must be of same length.\
                        Got lengths {lengths} for keys {data.keys()}'
                )
            length = lengths[0]

        # Make sure shape is always (length, -1)
        for k, v in data.items():
            if k not in keys_select:
                continue
            if isinstance(v, single_types):
                out_dict[k] = [v]
            elif isinstance(v, np.ndarray):
                if len(v.shape) == 1:
                    out_dict[k] = v.flatten().tolist()
                else:
                    # out_dict[k] = v.reshape(np.prod(v.shape)).tolist()
                    out_dict[k] = v.reshape((length, -1)).tolist()

            elif isinstance(v, List):
                vv = v.copy()
                for i, el in enumerate(vv):
                    if isinstance(el, array_types):
                        if len(el) == 1:
                            vv[i] = el[0]
                        else:
                            # flatten using numpy
                            vv[i] = np.array(el).flatten().tolist()
                out_dict[k] = vv
                # s = np.array(vv).shape
                # while len(s) > 2:
                #     nl = self.get_nest_level(vv)
                #     vv = self.flatten_numpy_array_at_level(vv, nl-1)
                #     s = np.array(vv).shape
                # out_dict[k] = vv
            else:
                print(f'Unsupported type in dict for key {k}: {type(v)}')
                continue
                # raise ValueError(
                #     f'Unsupported type in \
                #                  dict for key {k}: {type(v)}'
                # )

        for k, v in out_dict.items():
            if len(v) != 1 and length == 1:
                out_dict[k] = [v]
        return pd.DataFrame(out_dict)

    @get_data.register(Path)
    @get_data.register(str)
    def _(self, data: Union[str, Path], **kwargs):
        """Get data from str.

        Parameter
        ---------
        input_str: str
            input string (name of file or directory)
        options: dict
            options

        Returns
        -------
        data_type
            data as read from file or dir

        """
        try:
            path = Path(data)
        except (TypeError, ValueError):
            raise ValueError(f'could not convert {data} to Path')
        try:
            ex = path.exists()
        except (OSError, TypeError):
            raise ValueError(f'could not convert {data} to Path')

        if not ex:
            raise FileNotFoundError(f'could not find file {data}')

        if path.is_dir():
            return self.get_data_from_dir(path, **kwargs)
        else:
            return self.get_data_from_file(path, **kwargs)

    def get_data_from_dir(self, path: Path, **kwargs) -> data_type:
        """Read data from all files in dir.

        Parameter
        ---------
        path: :obj:`pathlib.Path`
            path to directory

        Returns
        -------
        list
            list of data read

        """
        matchlist = kwargs.pop('match', '*')

        if isinstance(matchlist, list):
            data = []
            for m in matchlist:
                d = self.get_data(path, match=m, **kwargs)
                d = self.combine(d, options={'align': 'rows'})  # type: ignore
                data.append(d)
            data = self.combine(  # type: ignore
                data, options={'align': 'columns'}
            )
            return data

        if not isinstance(matchlist, str):
            raise ValueError(f'invalid matchlist {matchlist}')

        return [
            self.get_data_from_file(f, **kwargs)
            for f in sorted(path.glob(matchlist))
        ]

    def get_data_from_parser(self, parser, path: Path, **kwargs):
        """Read data from file.

        Parameters
        ----------
        parser: callable
            parser function
        path: :obj:`pathlib.Path`
            path to datafile

        Returns
        -------
        np.ndarray or pd.DataFrame or list or dict
            data in appropriate format

        """
        data_dict: dict
        try:
            data_dict = (
                parser.parse(path)
                if hasattr(parser, 'parse')
                else parser(path)
            )
            if (
                kwargs.get('del_full_output', True)
                and 'full_output' in data_dict.keys()
            ):
                del data_dict['full_output']
        except Exception:
            raise RuntimeError(
                f'could not parse file {path} ' + 'with parser {parser}'
            )
        else:
            return self.get_data(data_dict, **kwargs)

    def get_data_from_file(self, path: Path, **kwargs):
        """Read data from file.

        Parameters
        ----------
        path: :obj:`pathlib.Path`
            path to datafile

        Returns
        -------
        np.ndarray or pd.DataFrame or list or dict
            data in appropriate format

        """
        data: data_type = None

        parser = kwargs.pop('parser', None)
        if parser:
            return self.get_data_from_parser(parser, path, **kwargs)

        if_mapping: Dict[str, Any] = {
            '.npy': NumpyInterface,
            '.csv': PandasInterface,
            '.json': PandasInterface,
            '.h5': PandasInterface,
            '.pkl': PandasInterface,
            '.parquet': PandasInterface,
            '.xyz': TrajectoryInterface,
        }

        interface: BaseInterface = None

        try:
            interface = if_mapping[path.suffix]()
        except KeyError as e:
            if data is None:
                raise NotImplementedError(
                    f'no interface for extension {path.suffix} available'
                ) from e
        else:
            data = interface.get_data(path, options=kwargs)

        original_shape = interface.get_shape(data)
        target_shape = kwargs.get('shape')
        if target_shape is not None:
            data = interface.rearrange(data, original_shape, target_shape)

        dimension = kwargs.get('dimension')
        name = kwargs.get('name', path.stem)
        return interface.to_pandas(data, dimension, name)

    def get_nest_level(self, data: Any) -> int:
        """Get the nest level of data.

        Parameters
        ----------
        data: Any
            data to check

        Returns
        -------
        int
            nest level

        """
        if isinstance(data, (list, np.ndarray)):
            return 1 + self.get_nest_level(data[0])
        return 0

    def flatten_numpy_array_at_level(
        self, arr: Union[List, np.ndarray], level: int
    ) -> list:
        """Flatten a numpy array at a specific level.

        This function takes a numpy array and flattens it at a specific level.

        Parameters
        ----------
        arr : list or numpy.ndarray
            The numpy array to flatten.
        level : int
            The level at which to flatten the array. Level 0 means no
            flattening, level 1 means flattening the last dimension into
            the second last, etc.

        Returns
        -------
        numpy.ndarray
            The flattened numpy array.

        Examples
        --------
        >>> arr = np.zeros((4, 3, 2, 1))
        >>> flatten_numpy_array_at_level(arr, 1).shape
        (4, 3, 2)
        >>> flatten_numpy_array_at_level(arr, 2).shape
        (4, 6)
        >>> flatten_numpy_array_at_level(arr, 3).shape
        (24,)

        """
        arr = np.array(arr)
        if level == 0:
            return arr.tolist()
        else:
            new_shape = arr.shape[:-level] + (-1,)
            return arr.reshape(new_shape).tolist()
