# movie tools
from functools import partial, wraps
from pathlib import Path
from typing import Callable, List, Optional, Union

import numpy as np

try:
    import plotly.graph_objects as go
except ImportError:
    go = None

from .plotly_camera import Camera
from .plotly_figure import PlotlyFigure

try:
    import moviepy.editor as mpy
except ImportError:
    mpy = None

try:
    import imageio as iio
except ImportError:
    iio = None

movie_menu = {
    'buttons': [{
        'args': [
            None, {
                'frame': {
                    'duration': None,
                    'redraw': True
                },
                'fromcurrent': True,
                'transition': {
                    'duration': 0,
                    'easing': 'quadratic-in-out'
                }
            }
        ],
        'label':
        'Play',
        'method':
        'animate'
    }, {
        'args': [[None], {
            'frame': {
                'duration': 0,
                'redraw': True
            },
            'mode': 'immediate',
            'transition': {
                'duration': 0
            }
        }],
        'label':
        'Pause',
        'method':
        'animate'
    }],
    'direction':
    'left',
    'pad': {
        'r': 10,
        't': 87
    },
    'showactive':
    False,
    'type':
    'buttons',
    'x':
    0.1,
    'xanchor':
    'right',
    'y':
    0,
    'yanchor':
    'top'
}


class PlotlyMovie(Camera, PlotlyFigure):
    """Plotly movie class."""

    @staticmethod
    def moviemaker(func: Callable):
        """Make movie from function."""
        @wraps(func)
        def wrapper(self, arg, *args, **kwargs):
            """Wrap function."""
            if not isinstance(arg, list):
                return func(self, arg, *args, **kwargs)
            else:
                ref_shape = np.array(arg[0]).shape
                if any(np.array(a).shape != ref_shape for a in arg):
                    raise ValueError('All arguments must have the same shape')
            try:
                style = args[0]
            except IndexError:
                style = None
            options = kwargs.pop('options', {})
            frame_duration = options.pop('frame_duration', 1)
            frame_slice = options.get('frame_slice', 1)
            save_movie = options.get('save_movie', False)
            resolution = options.get('resolution', 5)

            nframes = len(arg[0::frame_slice])
            camera = options.get('camera', self.default_camera)
            camera_ride = self.gen_camera_ride(camera, nframes)

            opt = options.copy()
            opt['resolution'] = resolution

            frames: List[go.Figure] = []  # type: ignore
            xmin, xmax, ymin, ymax, zmin, zmax = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
            for i, density in enumerate(arg[0::frame_slice]):
                opt['camera'] = camera_ride[i]
                frame: go.Figure = func(self, density, *args,  # type: ignore
                                        options=opt, **kwargs)
                frames.append(frame)
                x1, x2, y1, y2, z1, z2 = self.plotly_get_bounds_data(frame)
                xmin, xmax = min(xmin, x1), max(xmax, x2)
                ymin, ymax = min(ymin, y1), max(ymax, y2)
                zmin, zmax = min(zmin, z1), max(zmax, z2)

            frames = [
                self.plotly_update_frame_bounds(frame, style, xmin, xmax, ymin,
                                                ymax, zmin, zmax)
                for frame in frames
            ]

            if save_movie:
                return self.save_mp4(save_movie, frames)

            fig = frames[0]
            movie_menu['buttons'][0]['args'][1]['frame'][
                'duration'] = frame_duration
            fig.update_layout(updatemenus=[movie_menu])
            # fig.update_layout(transition = {'duration': frame_duration})
            fig.frames = [
                go.Frame(data=frame.data, layout=frame.layout)
                for frame in frames[1:]
            ]

            return fig

        return wrapper

    def make_frames_moviepy(
            self,
            t: float,
            frames: Optional[List[np.ndarray]] = None) -> np.ndarray:
        """Make frames for moviepy."""
        return frames[int(t)]

    def save_mp4(self, filename: Union[bool, str, Path],
                 frames: List[np.ndarray], **kwargs) -> None:
        """Save an animation to an mp4 file.

        Parameters
        ----------
        filename : Union[bool, str, Path]
            The desired filename for the mp4. If provided a bool, the
            filename will default to 'mymovie'. If the filename already
            exists with a '.mp4' suffix, a FileExistsError is raised.

        frames : List[np.ndarray]
            A list of numpy arrays representing the frames for the
            animation.

        **kwargs : dict, optional
            Additional keyword arguments. Supports:
                - fps (int): Frames per second. Default is 1.

        Raises
        ------
        ImportError:
            If 'imageio' is not installed.

        FileExistsError:
            If a file with '.mp4' suffix already exists.

        Notes
        -----
        Converts frames using `plotly_to_array` before saving.

        Examples
        --------
        >>> save_mp4("animation", frames_list)
        Saves the animation to 'animation.mp4'.

        >>> save_mp4(True, frames_list)
        Saves the animation to 'mymovie.mp4'.

        """
        if iio is None:
            raise ImportError('imageio is not installed')

        if isinstance(filename, bool):
            filename = 'mymovie'

        filename = Path(filename)
        if filename.with_suffix('.mp4').exists():
            raise FileExistsError(f'File {filename.with_suffix(".mp4")} ' +
                                  'already exists')

        fps = kwargs.get('fps', 1)
        _frames = [self.plotly_to_array(frame) for frame in frames]

        file = str(filename.with_suffix('.mp4'))
        with iio.get_writer(file, fps=fps) as writer:
            for frame in _frames:
                writer.append_data(frame)

    def save_mp4_moviepy(self, filename: Union[bool, str, Path],
                         frames: List[np.ndarray], **kwargs) -> None:
        """Save animation to mp4 file using moviepy."""
        if mpy is None:
            raise ImportError('moviepy is not installed')

        if isinstance(filename, bool):
            filename = 'mymovie'

        filename = Path(filename)
        if filename.with_suffix('.mp4').exists():
            raise FileExistsError(f'File {filename.with_suffix(".mp4")} ' +
                                  'already exists')

        fps = kwargs.get('fps', 1)

        _frames = [self.plotly_to_array(frame) for frame in frames]

        animation = mpy.VideoClip(partial(self.make_frames_moviepy,
                                          frames=_frames),
                                  duration=len(_frames))
        animation.write_gif(str(filename.with_suffix('.gif')), fps=fps)
        clip = mpy.VideoFileClip(str(filename.with_suffix('.gif')))
        clip.write_videofile(str(filename.with_suffix('.mp4')))
        return None
