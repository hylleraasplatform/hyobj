from functools import singledispatchmethod
from typing import Dict, List, Optional, Union

import numpy as np


class Camera:
    """Camera class for plotly."""

    @property
    def default_camera(self) -> dict:
        """Return default camera."""
        return {
            'up': {
                'x': 0,
                'y': 0,
                'z': 1
            },
            'center': {
                'x': 0,
                'y': 0,
                'z': 0
            },
            'eye': {
                'x': 1.25,
                'y': 1.25,
                'z': 1.25
            }
        }

    def spherical_to_cartesian(self, r: float, theta: float,
                               phi: float) -> np.ndarray:
        """Convert spherical to cartesian coordinates."""
        return np.array([
            r * np.sin(theta) * np.cos(phi), r * np.sin(theta) * np.sin(phi),
            r * np.cos(theta)
        ])

    def cartesian_to_spherical(self, x: float, y: float,
                               z: float) -> np.ndarray:
        """Convert cartesian to spherical coordinates."""
        r = np.linalg.norm([x, y, z])
        theta = np.arccos(z / r)
        phi = np.arctan2(y, x)
        return r, theta, phi

    def update_camera_key(self, camera: dict, key: str,
                          value: Union[dict, list, np.ndarray]) -> dict:
        """Update camera key."""
        if isinstance(value, (list, np.ndarray)):
            value = dict(zip(['x', 'y', 'z'], np.array(value).tolist()))
        camera[key] = value
        return camera

    def parse_camera(self, camera: Union[dict, list, np.ndarray]) -> dict:
        """Parse camera."""
        _camera = self.default_camera.copy()
        if isinstance(camera, (list, np.ndarray)):
            _camera['eye']['x'] = camera[0]
            _camera['eye']['y'] = camera[1]
            _camera['eye']['z'] = camera[2]
        elif isinstance(camera, dict):
            if 'up' in camera.keys():
                self.update_camera_key(_camera, 'up', camera['up'])
            if 'center' in camera.keys():
                self.update_camera_key(_camera, 'center', camera['center'])
            if 'eye' in camera.keys():
                self.update_camera_key(_camera, 'eye', camera['eye'])
        return _camera

    @singledispatchmethod
    def gen_camera_ride(self, camera: Union[dict, list],
                        n: int) -> List[Dict['str', 'dict']]:
        """Generate camera ride."""
        raise TypeError('camera must be a dictionary or list of ' +
                        'dictionaries')

    @gen_camera_ride.register
    def _(self, camera: dict, n: int) -> List[Dict]:
        """Generate camera ride."""
        if all(k in ['up', 'center', 'eye'] for k in camera.keys()):
            return [camera] * n

        if all(k in ['start', 'end'] for k in camera.keys()):
            start = self.parse_camera(camera['start'])
            end = self.parse_camera(camera['end'])
            return self.interpolate_cameras(
                start, end, n, camera.get('interpolation', 'spherical'))
        if 'rotate' in camera.keys():
            vec = self.parse_camera(camera)
            return self.rotate_camera(vec, camera['rotate'], n)

        raise ValueError('camera must be a dictionary or list of ' +
                         'dictionaries with keys up, center, eye, ' +
                         'start, end, or rotate')

    @gen_camera_ride.register
    def _(self, camera: list, n: int) -> List[Dict]:
        """Generate camera ride."""
        if len(camera) != n:
            raise ValueError('camera list must be same length as number ' +
                             f'of frames {len(camera)} vs {n}')
        return camera

    def rotate_camera(self, camera: dict, angle: Union[bool, float],
                      n: int) -> List[Dict]:
        """Rotate camera around up vector."""
        if not angle:
            return [camera] * n

        _angle = 2 * np.pi if isinstance(angle, bool) else angle
        if _angle > 2 * np.pi:
            _angle *= np.pi / 180.0

        angles = np.linspace(0, _angle, n)
        axis = np.array(
            [camera['up']['x'], camera['up']['y'], camera['up']['z']])
        vector = np.array(
            [camera['eye']['x'], camera['eye']['y'], camera['eye']['z']])
        cameras = [{
            'up':
            camera['up'],
            'center':
            camera['center'],
            'eye':
            dict(
                zip(['x', 'y', 'z'],
                    self.rotate_around_vector(axis, vector, phi)))
        } for phi in angles]
        return cameras

    def rotate_around_vector(self, axis: np.ndarray, vector: np.ndarray,
                             angle: float) -> np.ndarray:
        """Rotate vector around axis by angle."""
        axis = axis / np.linalg.norm(axis)
        vector = vector / np.linalg.norm(vector)
        v = vector * np.cos(angle) + np.cross(axis, vector) * np.sin(angle) + \
            axis * np.dot(axis, vector) * (1 - np.cos(angle))
        return v * np.linalg.norm(vector)

    def interpolate_cameras(
            self,
            start: dict,
            end: dict,
            n: int,
            interpolation_type: Optional[str] = 'spherical') -> List[Dict]:
        """Interpolate between two cameras."""
        start_up = np.array(
            [start['up']['x'], start['up']['y'], start['up']['z']])
        end_up = np.array([end['up']['x'], end['up']['y'], end['up']['z']])
        start_center = np.array(
            [start['center']['x'], start['center']['y'], start['center']['z']])
        end_center = np.array(
            [end['center']['x'], end['center']['y'], end['center']['z']])
        start_eye = np.array(
            [start['eye']['x'], start['eye']['y'], start['eye']['z']])
        end_eye = np.array([end['eye']['x'], end['eye']['y'], end['eye']['z']])

        up_vec = [
            list(start_up + (end_up - start_up) * i)
            for i in np.linspace(0, 1, n)
        ]

        center_vec = [
            list(start_center + (end_center - start_center) * i)
            for i in np.linspace(0, 1, n)
        ]

        if interpolation_type.lower() == 'linear':
            eye_vec = [
                list(start_eye + (end_eye - start_eye) * i)
                for i in np.linspace(0, 1, n)
            ]
        elif interpolation_type.lower() == 'spherical':
            eye_start_spherical = self.cartesian_to_spherical(*start_eye)
            eye_end_spherical = self.cartesian_to_spherical(*end_eye)
            eye_vec = [
                list(self.spherical_to_cartesian(r, theta, phi))
                for r, theta, phi in zip(
                    np.linspace(eye_start_spherical[0], eye_end_spherical[0],
                                n),
                    np.linspace(eye_start_spherical[1], eye_end_spherical[1],
                                n),
                    np.linspace(eye_start_spherical[2], eye_end_spherical[2],
                                n))
            ]
        else:
            raise ValueError('interpolation_type must be linear or spherical')

        cameras = [{
            'up': dict(zip(['x', 'y', 'z'], up_vec[i])),
            'center': dict(zip(['x', 'y', 'z'], center_vec[i])),
            'eye': dict(zip(['x', 'y', 'z'], eye_vec[i]))
        } for i in range(n)]
        return cameras
