import inspect
import warnings
from pathlib import Path
from typing import Any, Callable

import numpy as np

try:
    import pandas as pd
except ImportError:
    pd = None

from ..dataset import DataSet
from .abc import ConvertClassInstance
from .converter import Converter
from .molecule_readers import FILE_READERS, MoleculeReaders
from .utils import is_file, zmat2dict


class Constructor:
    """Constructors for Molecule objects."""

    def __init__(self):
        """Set subclass attributes."""
        self.optimize_smiles: str
        self.filetype: str

    def get_molecule_constructor(self, input: Any, **kwargs
                                 ) -> Callable[..., dict]:
        """Select correct molecule constructor by analysing input.

        Parameters
        ----------
        input : :obj:`Any`
            molecular input (str, list, dict, or class instance)

        Returns
        -------
        :obj:`Callable`
            method that constructs a molecule from input

        Note
        ----
        We tried to balance user-friendliness with SOLID programming.
        Therefore, most of the parsing is condensed in this routine.

        """
        debug = kwargs.get('debug', False)
        # case i : filename or explicit molecule input
        if isinstance(input, str):
            if debug:
                print('input is a string')
            return self.get_mol_from_str(input)

        # case ii : list
        elif isinstance(input, list):
            if debug:
                print('input is a list')
            # case a : zmat as a list of strings beginning with a single
            # element symbol

            if len(input[0]) == 1:
                if len(input) == 4:
                    if isinstance(input[1], list):
                        if isinstance(input[1][0], str):
                            self.filetype = 'zmat'
                            return zmat2dict
                        else:
                            raise NotImplementedError(
                                'Could not generate molecule constructor ' +
                                f'from input {input}')
                    elif (isinstance(input[1], float)
                          or isinstance(input[1], int)
                          or isinstance(input[1], np.float64)
                          or isinstance(input[1], np.int64)):
                        # single-atom case Molecule([a1, x1, y1, z1])
                        self.filetype = 'xyz'
                        return MoleculeReaders.get_molecule_from_list_xyz_1
                    # else:
                    #     raise NotImplementedError(
                    #         f'Could not generate molecule constructor from
                    # input {input}')
                    # try:
                    #     float(input[1])
                    # except Exception:
                    #     self.filetype = 'zmat'
                    #     return zmat2xyz
                    # else:
                    #     self.filetype = 'xyz'
                    #     return MoleculeReaders.get_molecule_from_list_xyz_1
                # else:

                #     if len(input) > 1:
                #         if isinstance(input[1][0], str):
                #             self.filetype = 'zmat'
                #             return zmat2xyz
                #         else :
                #             self.filetype = 'xyz'
                #           return MoleculeReaders.get_molecule_from_list_xyz_1
                #     else:
                #         raise InputError(f'could not read molecule from input
                # {input} (neither zmat nor xyz)')

            # case b : xyz in format [ [atom1,x1,y1,z1], [atom2,...] ]
            if all(len(sublist) == 4 for sublist in input):
                self.filetype = 'xyz'
                # 4 or 1 atoms case
                if all(isinstance(elem, str) for elem in input[0]):
                    return MoleculeReaders.get_molecule_from_list_xyz_0
                else:
                    return MoleculeReaders.get_molecule_from_list_xyz_1
            if len(input) > 2 and len(input[2]) == 5:
                self.filetype = 'zmat'
                return zmat2dict

            if len(input) == 2 and len(input[0]) == 1:
                self.filetype = 'zmat'
                return zmat2dict
            # case c : xyz in the format [atoms, cartesian coordinates]
            if (len(input) > 0
                    and len(input[0]) == sum(map(len, input[1])) // 3):
                self.filetype = 'xyz'
                return MoleculeReaders.get_molecule_from_list_xyz_0

        # case iii : dictionary
        elif isinstance(input, dict):
            if debug:
                print('input is a dictionary')
            return MoleculeReaders.get_molecule_from_dict

        else:
            # case iv : class
            self.filetype = None
            if inspect.isclass(type(input)):
                if debug:
                    print('input is a class')
                # case a : support for self
                converter: ConvertClassInstance = None

                if (hasattr(input, 'atoms') and hasattr(input, 'coordinates')):
                    converter = Converter.ConvertHylleraasMolecule()
                # case b : support for daltonproject
                elif hasattr(input, 'elements') and hasattr(
                        input, 'coordinates'):
                    converter = Converter.ConvertDaltonprojectMolecule()

                if pd is not None:
                    if isinstance(input, (pd.DataFrame, pd.Series, DataSet)):
                        converter = Converter.ConvertDataSetMolecule()

                if converter is None:
                    raise TypeError(
                        'Can only convert molecule objects with attribute ' +
                        '"atoms" and "coordinates"')
                if debug:
                    print('converter', converter)

                return converter.get_molecule_from_class_instance

        # # case v : not implemented
        # raise NotImplementedError(
        #      f'Could not generate molecule constructor from input {input}')
        return None

    def get_mol_from_str(self, input: str
                         ) -> Callable[..., dict]:
        """Get molecule constructor from string input.

        Parameter
        ---------

        input: str
            molecule input

        Returns
        -------
        Callable
            corresponding constructor method

        """
        if is_file(input):
            return self.get_mol_from_file(Path(input))

        if self.filetype is None:
            # case 0 check if we have a smiles string
            try:
                from rdkit import Chem, RDLogger
            except Exception:
                warnings.warn('could not fine package rdkit')
            else:
                RDLogger.DisableLog('rdApp.*')
                m = Chem.MolFromSmiles(input, sanitize=False)
                RDLogger.EnableLog('rdApp.*')
                if all([
                        m is not None,
                        '\n' not in input,
                        ' ' not in input,
                ]):
                    if self.optimize_smiles != '':
                        if self.optimize_smiles.lower() == 'mmff':
                            return (
                                MoleculeReaders.
                                get_molecule_from_string_smiles_mmff)
                        else:
                            raise NotImplementedError(
                                'Unrecognized SMILES optimization '
                                f'{self.optimize_smiles}')
                    return (MoleculeReaders.
                            get_molecule_from_string_smiles)

            # default case, e.g. xyz file without the first two lines:
            # H 0 0 0
            lines = list(input.strip().split('\n'))
            # case 1 : (normal xyz-format) first element is a string
            #           of digits (number of atoms)
            # case 2 : (normal zmat-format) first element is a string
            #            of strings (first atom/elementsymbol)
            if lines[0].count(' ') == 0 and not lines[0].isdigit():
                return MoleculeReaders.get_molecule_from_string_zmat
            else:
                return MoleculeReaders.get_molecule_from_string_xyz

        elif self.filetype == 'zmat':
            return MoleculeReaders.get_molecule_from_string_zmat
        elif self.filetype == 'xyz':
            return MoleculeReaders.get_molecule_from_string_xyz
        elif self.filetype.lower() == 'smiles':
            if self.optimize_smiles.lower() == 'mmff':
                return (MoleculeReaders.
                        get_molecule_from_string_smiles_mmff)
            else:
                return MoleculeReaders.get_molecule_from_string_smiles
        else:
            raise TypeError(
                f'Cannot read filetype {self.filetype} format ' +
                ' from string {input}')

    def get_mol_from_file(self, filename: Path
                          ) -> Callable[..., dict]:
        """Get constructor for reading molecule from file.

        Parameter
        ---------
        input: Path
            filemame

        Returns
        -------
        Callable
            corresponding reader routine

        """
        self.filename = filename
        self.filetype = (self.filename.suffix
                         if self.filetype is None else self.filetype)

        if self.filetype in FILE_READERS:
            reader = getattr(
                MoleculeReaders, FILE_READERS[self.filetype.lower()])()
            return reader.get_molecule_from_file
        try:
            import hyif
            reader = getattr(getattr(hyif, hyif.PROGRAMS[self.filetype]),
                             'get_input_molecule')
            return reader
        except Exception:
            reader = getattr(
                MoleculeReaders, FILE_READERS['.ase'])()
            return reader.get_molecule_from_file
