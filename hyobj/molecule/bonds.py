import warnings

import numpy as np
from qcelemental import covalentradii


class Bonds:
    """Tools for Bond analysis."""

    def __init__(self):

        self._coordinates: np.ndarray
        self._atoms: list
        self._properties: dict

    @property
    def bonds(self) -> list:
        """Compute bonds with lengths."""
        # unit = self._properties['unit']
        unit = self.units.length[0]  # type: ignore
        fac = 1.05  # Turbomole definition
        bonds = []
        for i, atom1 in enumerate(self._atoms):
            try:
                r1 = covalentradii.get(atom1, units=unit)
            except Exception:
                raise ValueError('could not find covalent radius for ' +
                                 f'{atom1} in {unit}')
            x1 = self._coordinates[i]
            for j in range(i + 1, len(self._atoms)):
                atom2 = self._atoms[j]
                r2 = covalentradii.get(atom2, units=unit)
                x2 = self._coordinates[j]
                dist = np.linalg.norm(x1 - x2)
                if dist < fac * (r1 + r2):
                    bonds.append([i, j, dist])
        self._bonds = bonds
        return bonds

    @property
    def bond_orders(self) -> list:
        """Get bond orders from xyz2mol (see 10.1002/bkcs.10334)."""
        try:
            import xyz2mol as xyz2mol_jensen
        except ImportError:
            warnings.warn('could not find xyz2mol see 10.1002/bkcs.10334')
            return []

        charge = self._properties.get('charge', 0)
        coordinates = self._coordinates
        atoms = [xyz2mol_jensen.int_atom(atom.lower()) for atom in self._atoms]

        ac, _ = xyz2mol_jensen.xyz2AC(atoms,
                                      coordinates,
                                      charge,
                                      use_huckel=False)
        bo, _ = xyz2mol_jensen.AC2BO(ac,
                                     atoms,
                                     charge,
                                     allow_charged_fragments=True,
                                     use_graph=True)

        return bo
