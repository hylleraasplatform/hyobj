from typing import Any

import numpy as np

try:
    import pandas as pd
except ImportError:
    pd = None

from ..dataset import DataSet
from .abc import ConvertClassInstance


class Converter:
    """Collection of Molecule converters."""

    class ConvertHylleraasMolecule(ConvertClassInstance):
        """Convert hylleraas.Molecule instances and compatible."""

        def get_molecule_from_class_instance(
                self, molecule: Any) -> dict:
            """Extract molecule from Hylleraas-compatible class instance.

            Parameters
            ----------
            molecule : :obj:`Any`
                (Hylleraas-like) class instance

            Returns
            -------
            dict
                list of atoms and coordinates, property dictionary

            """
            return molecule.__dict__

        @classmethod
        def get_properties_from_class_instance(cls, molecule: Any) -> dict:
            """Extract molecule properties from Hylleraas-compatible class.

            Parameters
            ----------
            molecule : :obj:`Any`
                (Hylleraas-like) class instance

            Returns
            -------
            dict
                all properties

            """
            return molecule.properties

    class ConvertDaltonprojectMolecule(ConvertClassInstance):
        """Convert daltonproject.Molecule instances and compatible."""

        def get_molecule_from_class_instance(
                self, molecule: Any) -> dict:
            """Extract molecule from daltonproject-compatible class instance.

            Parameters
            ----------
            molecule : :obj:`Any`
                (daltonproject-like) class instance

            Returns
            -------
            dict
                atoms and coordinates, property dictionary

            """
            return {'atoms': molecule.elements,
                    'coordinates': molecule.coordinates}

        def get_properties_from_class_instance(self, molecule: Any) -> dict:
            """Extract molecule properties from daltonproject.

            Parameters
            ----------
            molecule : :obj:`Any`
                (daltonproject-like) class instance

            Returns
            -------
            dict
                charge and point_group

            """
            return {
                'charge': molecule.charge,
                'point_group': molecule.point_group,
            }

    class ConvertDataSetMolecule(ConvertClassInstance):
        """Convert DataSet instances and compatible."""

        def get_molecule_from_class_instance(
                self, molecule: Any) -> dict:
            """Extract molecule from daltonproject-compatible class instance.

            Parameters
            ----------
            molecule : :obj:`Any`
                DataSet row

            Returns
            -------
            dict
                atoms and coordinates, property dictionary

            """
            if pd is None:
                raise ImportError('could not find package pandas')

            if isinstance(molecule, pd.DataFrame):
                data = molecule
            elif isinstance(molecule, DataSet):
                data = molecule.data
            elif isinstance(molecule, pd.Series):
                data = molecule.data.to_frame().T
            else:
                raise TypeError('expected pandas data type(s)')

            atoms: list = []
            coordinates: list = []
            clist = ['coordinates', 'coord', 'coords', 'xyz']
            for c in list(data.columns):
                if c in clist:
                    coordinates = data[c].to_numpy()[0]
                    break

            alist = ['atoms', 'elements']
            for c in list(data.columns):
                if c in alist:
                    atoms = data[c].values[0]  # .tolist()
                    break

            # return molecule.elements, molecule.coordinates
            return {'atoms': atoms,
                    'coordinates': np.array(coordinates).reshape(-1, 3)}

        def get_properties_from_class_instance(self, molecule: Any) -> dict:
            """Extract molecule properties from DatSet.

            Parameters
            ----------
            molecule : :obj:`Any`
                DataSet row

            Returns
            -------
            dict
                properties like unit_cell and so on

            """
            if pd is None:
                raise ImportError('could not find package pandas')
            if isinstance(molecule, pd.DataFrame):
                data = molecule
            elif isinstance(molecule, DataSet):
                data = molecule.data
            elif isinstance(molecule, pd.Series):
                data = molecule.data.to_frame().T
            else:
                raise TypeError('expected pandas data type(s)')

            pbclist = ['box', 'unit_cell']
            properties = {}
            for c in list(data.columns):
                if c in pbclist:
                    properties['unit_cell'] = data[c].values[0]
                    break

            return properties
