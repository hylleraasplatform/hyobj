from typing import Optional, Union

import numpy as np

from ..constants import Constants
from ..units import Units

try:
    import geometric
except ImportError:  # pragma no cover
    geometric = None


class InternalCoordinates:
    """Tools for internal coordinates."""

    def __init__(self):
        """Set subclass attributes."""
        self.num_atoms: int
        self.coordinates: np.ndarry
        self._properties: dict

    def distance(self, i1: int, i2: int,
                 units: Optional[Units] = None) -> float:
        """Compute distance in hylleraas Molecule instance.

        .. math::
            d = ||v_{i1} - v_{i2}||_2

        the vectors will be taken from `self.coordinates

        Parameters
        ----------
        i1 : int
            index of atom #1 in self.Molecule instance
        i2 : int
            index of atom #2 in self.Molecule instance
        units : Units
            units to return distance in

        Returns
        -------
        float
            distance :math:`d between atoms i1 and i2 in bohr

        """
        for index in i1, i2:
            if index < 0 or index > self.num_atoms - 1:
                raise ValueError('Index not in 0, num_atoms-1!')
        v1 = np.array(self.coordinates[i1])
        v2 = np.array(self.coordinates[i2])
        val = np.linalg.norm(v1 - v2)
        if units is not None:
            val *= units.length[1]  # type: ignore
        return val

    def angle(self, i1: int, i2: int, i3: int) -> float:
        """Compute angle in hylleraas Molecule instance.

        Parameters
        ----------
        i1 : int
            index of atom #1 in self.Molecule instance
        i2 : int
            index of atom #2 in self.Molecule instance
        i3 : int
            index of atom #3 in self.Molecule instance

        Returns
        -------
        float
            angle (i1,i2,i3) in degrees

        """
        for index in i1, i2, i3:
            if index < 0 or index > self.num_atoms - 1:
                raise ValueError('Index not in 0, num_atoms-1!')
        v1 = np.array(self.coordinates[i1]) - np.array(self.coordinates[i2])
        v2 = np.array(self.coordinates[i3]) - np.array(self.coordinates[i2])
        c = np.dot(v1, v2)
        s = np.linalg.norm(np.cross(v1, v2))
        val = np.arctan2(s, c) * 180.0 / np.pi
        return val

    def dihedral(self, i1: int, i2: int, i3: int, i4: int) -> float:
        """Compute dihedral angle in hylleraas Molecule instance.

        Parameters
        ----------
        i1 : int
            index of atom #1 in self.Molecule instance
        i2 : int
            index of atom #2 in self.Molecule instance
        i3 : int
            index of atom #3 in self.Molecule instance
        i4 : int
            index of atom #4 in self.Molecule instance

        Returns
        -------
        float
            dihedral angle (i1,i2,i3,i4) in degrees

        """
        for index in i1, i2, i3, i4:
            if index < 0 or index > self.num_atoms - 1:
                raise ValueError('Index not in 0, num_atoms-1!')
        u1 = np.array(self.coordinates[i2]) - np.array(self.coordinates[i1])
        u2 = np.array(self.coordinates[i3]) - np.array(self.coordinates[i2])
        u3 = np.array(self.coordinates[i4]) - np.array(self.coordinates[i3])
        v1 = np.cross(u1, u2)
        v2 = -np.cross(u2, u3)
        n1 = np.linalg.norm(v1)
        n2 = np.linalg.norm(v2)
        n3 = np.linalg.norm(u2)
        if n1 < 1.0e-9 or n2 < 1.0e-9 or n3 < 1.0e-09:
            return 0.0
        v1 = v1 / n1
        v2 = v2 / n2
        w = np.cross(v1, u2)
        x = np.dot(v1, v2)
        y = np.dot(w, v2) / n3
        val = -(np.arctan2(y, x) + np.pi) * 180.0 / np.pi
        if val < -180.0:
            val += 360.0
        return val

    def get_distances(
        self,
        thresh_min: Optional[float] = None,
        thresh_max: Optional[float] = None,
        units: Optional[Union[str, Units]] = '',
        **kwargs
    ) -> list:
        """Compute distance matrix in hylleraas Molecule instance.

        Parameters
        ----------
        thresh_min : float, optional
            minimum value in output list, defaults to None.
        thresh_max : float, optional
            maximum value in output list, defaults to None.
        units : str, :obj:`hyobj.Units`, optional
            unit used, defaults to 'bohr'/atomic units

        Returns
        -------
        list
            distance matrix [[a1,a2,dist(a1,a2)]]

        """
        if kwargs.get('unit'):
            raise DeprecationWarning('use units instead of unit')
        import scipy.spatial.distance as gen_distmat

        distmat = gen_distmat.pdist(self.coordinates)

        if not units:
            units = Units()

        if isinstance(units, str):
            if units.lower() in 'bohrs':
                units = Units('atomic')
            elif units.lower() in 'angstroms':
                units = Units('metal')
            else:
                raise ValueError(f'unknown unit {units}')
        # unit == '':
        #     unit = self._properties.get('unit')
        if not hasattr(self, 'units'):
            raise AttributeError(f'units not set in {self}')
        unit_scale = units.length[1]/self.units.length[1]  # type: ignore

        # scale_mapping = {'bohr': 1.0, 'angstrom': constants.bohr2angstroms}
        # unit_scale = scale_mapping[unit]
        m = self.num_atoms
        thresh_max = 1e6 if thresh_max is None else thresh_max
        thresh_min = -1.0 if thresh_min is None else thresh_min

        distarray = []

        for i in range(0, m):
            for j in range(i + 1, m):
                pos = int(m * i + j - ((i + 1) * (i + 2)) / 2)
                val = distmat[pos] * unit_scale
                if val < thresh_min or val > thresh_max:
                    continue
                distarray.append([i, j, val])

        return distarray

    def get_internal_coordinates(self):
        """Compute delocalized internal coordinates using geometric."""
        internal_coords: dict = {}
        if geometric is None:
            raise ImportError('could not find package geometric')

        geometric_molecule = geometric.molecule.Molecule()
        geometric_molecule.elem = self.atoms
        geometric_molecule.xyzs = [
            np.array(self.coordinates) * Constants.bohr2angstroms
        ]

        ic = geometric.optimize.DelocalizedInternalCoordinates(
                geometric_molecule)

        defs = ic.Prims.Internals
        internal_coords['definitions'] = []
        for elem in defs:
            elem = str(elem).split()
            atoms = elem[1].split('-')
            atoms = [int(i) - 1 for i in atoms]
            internal_coords['definitions'].append([elem[0].lower(), *atoms])

        internal_coords['values'] = ic.Prims.calculate(
            geometric_molecule.xyzs[0])
        ic.build_dlc(geometric_molecule.xyzs[0])

        internal_coords['contracted'] = ic.Vecs
        internal_coords['num_primitives'] = np.shape(ic.Vecs)[0]
        internal_coords['num_contracted'] = np.shape(ic.Vecs)[1]
        internal_coords['3n-6'] = 3 * len(self.atoms) - 6

        return internal_coords
